/*
 * main.c
 *
 *  Created on: 22.10.2010
 *      Author: Sascha Springer
 *
 *
 * for file in *; do ./MsaTool --extract-files $file; done
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <sys/stat.h>

/*
 * Defines.
 */

#define READ_M8(address, offset) (address[offset])
#define READ_M16(address, offset) ((address[offset + 1] & 0xff) | ((address[offset] & 0xff) << 8))
#define READ_M32(address, offset) ((address[offset + 3] & 0xff) | ((address[offset + 2] & 0xff) << 8) | ((address[offset + 1] & 0xff) << 16) | ((address[offset] & 0xff) << 24))

#define READ_L8(address, offset) (address[offset])
#define READ_L16(address, offset) ((address[offset] & 0xff) | ((address[offset + 1] & 0xff) << 8))
#define READ_L32(address, offset) ((address[offset] & 0xff) | ((address[offset + 1] & 0xff) << 8) | ((address[offset + 2] & 0xff) << 16) | ((address[offset + 3] & 0xff) << 24))

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

#define MAX_PATH_SIZE 2048
#define BYTES_PER_SECTOR 512

/*
 * Types.
 */

typedef struct MsaImageInfo_t
{
	int iSectorSize;
	int iStartTrack;
	int iEndTrack;
	int iSectorsPerTrack;
	int iNumHeads;
	int iNumTracks;
	int iTotalSectors;
} MsaImageInfo_t;

typedef struct DiskGeometryInfo_t
{
	int iBytesPerSector;
	int iSectorsPerCluster;
	int iReservedSectors;
	int iNumberOfFats;
	int iMaxNumberOfRootEntries;
	int iTotalSectors;
	int iMediaDescriptor;
	int iSectorsPerFat;
	int iSectorsPerTrack;
	int iNumberOfSides;
	int iNumberOfHiddenSectors;
} DiskGeometryInfo_t;

typedef struct DirectoryEntry_t
{
	char tName[8 + 1];
	char tExtension[3 + 1];
	int iAttributes;
	int iTime;
	int iDate;
	int iStartCluster;
	int iSize;
} DirectoryEntry_t;

typedef enum ProgramArgumentType_t
{
	ARG_TYPE_NONE, ARG_TYPE_STRING, ARG_TYPE_INT
} ProgramArgumentType_t;

typedef struct ProgramArgumentInfo_t
{
	char tShortName[10];
	char tLongName[40];
	char tDescription[100];

	union
	{
		char *pValue;
		int iValue;
	} tValueUnion;

	ProgramArgumentType_t tType;
	int iIsSet;
} ProgramArgumentInfo_t;

/*
 * Members.
 */

unsigned char *m_pMsaImage = NULL;
unsigned char *m_pDiskImage = NULL;

ProgramArgumentInfo_t m_tProgramArguments[] =
{
	{ tShortName: "-x", tLongName: "--extract-files", tDescription: "Extract MSA image files.", tType: ARG_TYPE_STRING },
	{ tShortName: "-v", tLongName: "--list-files", tDescription: "List MSA image files.", tType: ARG_TYPE_NONE, iIsSet: 1 },
	{ tShortName: "-s", tLongName: "--show-info", tDescription: "Show MSA image info.", tType: ARG_TYPE_NONE },
	{ tShortName: "-o", tLongName: "--omit-msa-folder", tDescription: "Omit creating a folder with the MSA image name.", tType: ARG_TYPE_NONE },
	{ tShortName: "-c", tLongName: "--convert-to-st", tDescription: "Convert MSA image to an ST image.", tType: ARG_TYPE_STRING },
};

int m_iNumProgramArguments = sizeof(m_tProgramArguments) / sizeof(m_tProgramArguments[0]);

/*
 * Functions.
 */

int ParseArguments(int iNumOriginalArguments, char **ppOriginalArguments)
{
	int iOriginalIndex;
	int iProgramIndex;
	char *pArgument;
	int iArgumentSize;

	for(iOriginalIndex = 1; iOriginalIndex < iNumOriginalArguments; iOriginalIndex++)
	{
		for(iProgramIndex = 0; iProgramIndex < m_iNumProgramArguments; iProgramIndex++)
		{
			pArgument = strstr(ppOriginalArguments[iOriginalIndex], m_tProgramArguments[iProgramIndex].tShortName);

			if(pArgument && pArgument == ppOriginalArguments[iOriginalIndex])
			{
				iArgumentSize = strlen(pArgument) - strlen(m_tProgramArguments[iProgramIndex].tShortName);
				pArgument += strlen(m_tProgramArguments[iProgramIndex].tShortName);
			}
			else
			{
				pArgument = strstr(ppOriginalArguments[iOriginalIndex], m_tProgramArguments[iProgramIndex].tLongName);

				if(pArgument && pArgument == ppOriginalArguments[iOriginalIndex])
				{
					iArgumentSize = strlen(pArgument) - strlen(m_tProgramArguments[iProgramIndex].tLongName);
					pArgument += strlen(m_tProgramArguments[iProgramIndex].tLongName);

					if(iArgumentSize > 0 && *pArgument == '=')
					{
						pArgument++;
						iArgumentSize--;
					}
				}
			}

			if(pArgument)
			{
				switch(m_tProgramArguments[iProgramIndex].tType)
				{
				case ARG_TYPE_NONE:
					m_tProgramArguments[iProgramIndex].tValueUnion.iValue = 1;

					break;

				case ARG_TYPE_STRING:
					m_tProgramArguments[iProgramIndex].tValueUnion.pValue = pArgument;

					break;

				case ARG_TYPE_INT:
					m_tProgramArguments[iProgramIndex].tValueUnion.iValue = atoi(pArgument);

					break;

				default:
					break;
				}

				m_tProgramArguments[iProgramIndex].iIsSet = 1;
			}
		}
	}

	return 0;
}

void PrintUsage()
{
	int iIndex;
	char tArgumentString[60];

	for(iIndex = 0; iIndex < m_iNumProgramArguments; iIndex++)
	{
		strcpy(tArgumentString, m_tProgramArguments[iIndex].tLongName);

		switch(m_tProgramArguments[iIndex].tType)
		{
		case ARG_TYPE_NONE:
			break;

		case ARG_TYPE_STRING:
			strcat(tArgumentString, "=<string>");

			break;

		case ARG_TYPE_INT:
			strcat(tArgumentString, "=<integer>");

			break;

		default:
			break;
		}

		printf("%-25s ", tArgumentString);
		printf("(%s) ", m_tProgramArguments[iIndex].tShortName);
		printf("%s\n", m_tProgramArguments[iIndex].tDescription);
	}
}

int GetArgumentValue(char *pLongName, int iDefaultValue)
{
	int iIndex;

	for(iIndex = 0; iIndex < m_iNumProgramArguments; iIndex++)
		if(strcmp(m_tProgramArguments[iIndex].tLongName, pLongName) == 0 && m_tProgramArguments[iIndex].iIsSet)
			return m_tProgramArguments[iIndex].tValueUnion.iValue;

	return iDefaultValue;
}

int GetArgumentValueInt(char *pLongName, int iDefaultValue)
{
	int iIndex;

	for(iIndex = 0; iIndex < m_iNumProgramArguments; iIndex++)
		if(strcmp(m_tProgramArguments[iIndex].tLongName, pLongName) == 0 && m_tProgramArguments[iIndex].iIsSet)
			return m_tProgramArguments[iIndex].tValueUnion.iValue;

	return iDefaultValue;
}

char *GetArgumentValueString(char *pLongName, char *pDefaultValue)
{
	int iIndex;

	for(iIndex = 0; iIndex < m_iNumProgramArguments; iIndex++)
		if(strcmp(m_tProgramArguments[iIndex].tLongName, pLongName) == 0 && m_tProgramArguments[iIndex].iIsSet)
			return m_tProgramArguments[iIndex].tValueUnion.pValue;

	return pDefaultValue;
}

int IsArgumentSet(char *pLongName)
{
	int iIndex;

	for(iIndex = 0; iIndex < m_iNumProgramArguments; iIndex++)
		if(strcmp(m_tProgramArguments[iIndex].tLongName, pLongName) == 0 && m_tProgramArguments[iIndex].iIsSet)
			return m_tProgramArguments[iIndex].iIsSet;

	return 0;
}

DirectoryEntry_t ReadDirectoryEntry(unsigned char *pRawDirectoryEntry)
{
	DirectoryEntry_t tDirectoryEntry;
	int iCharIndex;

	memset(&tDirectoryEntry, 0, sizeof(tDirectoryEntry));

	for(iCharIndex = 0; iCharIndex < 8 && pRawDirectoryEntry[iCharIndex] != ' '; iCharIndex++)
		tDirectoryEntry.tName[iCharIndex] = pRawDirectoryEntry[iCharIndex];

	pRawDirectoryEntry += 8;

	for(iCharIndex = 0; iCharIndex < 3 && pRawDirectoryEntry[iCharIndex] != ' '; iCharIndex++)
		tDirectoryEntry.tExtension[iCharIndex] = pRawDirectoryEntry[iCharIndex];

	pRawDirectoryEntry += 3;

	tDirectoryEntry.iAttributes = *pRawDirectoryEntry++;

	pRawDirectoryEntry += 10;

	tDirectoryEntry.iTime = READ_L16(pRawDirectoryEntry, 0);
	pRawDirectoryEntry += 2;

	tDirectoryEntry.iDate = READ_L16(pRawDirectoryEntry, 0);
	pRawDirectoryEntry += 2;

	tDirectoryEntry.iStartCluster = READ_L16(pRawDirectoryEntry, 0);
	pRawDirectoryEntry += 2;

	tDirectoryEntry.iSize = READ_L32(pRawDirectoryEntry, 0);

	return tDirectoryEntry;
}

void PrintDirectoryEntry(DirectoryEntry_t *pDirectoryEntry, int iDepth)
{
	int iDepthIndex;
	int iTextSize;

	for(iDepthIndex = 0; iDepthIndex < iDepth; iDepthIndex++)
	{
		if(iDepthIndex == iDepth - 1)
			printf("+---");
		else
			printf("|   ");
	}

	printf("%s", pDirectoryEntry->tName);
	iTextSize = strlen(pDirectoryEntry->tName);

	if(pDirectoryEntry->tExtension[0] != '\0')
	{
		printf(".%s", pDirectoryEntry->tExtension);
		iTextSize += strlen(pDirectoryEntry->tExtension) + 1;
	}

	while(iTextSize < (30 - iDepth * 4))
	{
		printf(" ");
		iTextSize++;
	}

	if(pDirectoryEntry->iAttributes & 0x10)
		printf("<DIR>   ");
	else if(pDirectoryEntry->iAttributes & 0x08)
		printf("<VOL>   ");
	else
		printf("%8d", pDirectoryEntry->iSize);

	printf(" ");

	printf("%02d:%02d:%02d %02d.%02d.%4d",
		pDirectoryEntry->iTime >> 11, (pDirectoryEntry->iTime >> 5) & 0x3f, (pDirectoryEntry->iTime & 0x1f) << 1,
		pDirectoryEntry->iDate & 0x1f, (pDirectoryEntry->iDate >> 5) & 0x0f, ((pDirectoryEntry->iDate >> 9) & 0x7f) + 1980);

	printf("\n");
}

int GetNextCluster(DiskGeometryInfo_t *pDiskGeometryInfo, int iCurrentCluster)
{
	int iClusterIndex = (iCurrentCluster >> 1) * 3;
	int iClusterByte0 = m_pDiskImage[pDiskGeometryInfo->iBytesPerSector + iClusterIndex];
	int iClusterByte1 = m_pDiskImage[pDiskGeometryInfo->iBytesPerSector + iClusterIndex + 1];
	int iClusterByte2 = m_pDiskImage[pDiskGeometryInfo->iBytesPerSector + iClusterIndex + 2];

	if(iCurrentCluster & 1)
		iCurrentCluster = ((iClusterByte2 << 4) & 0xff0) | ((iClusterByte1 >> 4) & 0x00f);
	else
		iCurrentCluster = (iClusterByte0 & 0x0ff) | ((iClusterByte1 << 8) & 0xf00);

	return iCurrentCluster;
}

int IsNameValid(DirectoryEntry_t *pDirectoryEntry)
{
	int iSize;
	int iIndex;
	char cChar;

	iSize = strlen(pDirectoryEntry->tName);

	if(iSize == 0)
		return 0;

	for(iIndex = 0; iIndex < iSize; iIndex++)
	{
		cChar = pDirectoryEntry->tName[iIndex];

		if(cChar < 33 || cChar > 126)
			return 0;

		if(cChar == '<' || cChar == '>' || cChar == ':' || cChar == '"' || cChar == '/' ||
			cChar == '\\' || cChar == '|' || cChar == '?' || cChar == '*' || cChar == '.')
			return 0;
	}

	iSize = strlen(pDirectoryEntry->tExtension);

	for(iIndex = 0; iIndex < iSize; iIndex++)
	{
		cChar = pDirectoryEntry->tExtension[iIndex];

		if(cChar < 33 || cChar > 126)
			return 0;

		if(cChar == '<' || cChar == '>' || cChar == ':' || cChar == '"' || cChar == '/' ||
			cChar == '\\' || cChar == '|' || cChar == '?' || cChar == '*' || cChar == '.')
			return 0;
	}

	return 1;
}

int ExtractFile(DiskGeometryInfo_t *pDiskGeometryInfo, int iFileCluster, char *pFullFileName, int iFileSize)
{
	FILE *pFile;

	if(iFileSize <0 || iFileSize > pDiskGeometryInfo->iTotalSectors * pDiskGeometryInfo->iBytesPerSector)
	{
		printf("File broken (invalid size)!\n");

		return 1;
	}

	pFile = fopen(pFullFileName, "wb");

	if(pFile)
	{
		int iBytesWritten = 0;
		long lFileOffset;

		while(iFileCluster >= 0x002 && iFileCluster <= 0xfef && iBytesWritten < iFileSize)
		{
			lFileOffset =
				(pDiskGeometryInfo->iReservedSectors +
				pDiskGeometryInfo->iSectorsPerFat * pDiskGeometryInfo->iNumberOfFats +
				(pDiskGeometryInfo->iMaxNumberOfRootEntries >> 4) +
				(iFileCluster - 2) * pDiskGeometryInfo->iSectorsPerCluster) * pDiskGeometryInfo->iBytesPerSector;

			if(lFileOffset >= ((pDiskGeometryInfo->iTotalSectors - pDiskGeometryInfo->iSectorsPerCluster) * pDiskGeometryInfo->iBytesPerSector))
			{
				printf("File broken (invalid cluster)!\n");

				break;
			}

			fwrite(m_pDiskImage + lFileOffset, 1, MIN(pDiskGeometryInfo->iSectorsPerCluster * pDiskGeometryInfo->iBytesPerSector, iFileSize - iBytesWritten), pFile);

			iBytesWritten += MIN(pDiskGeometryInfo->iSectorsPerCluster * pDiskGeometryInfo->iBytesPerSector, iFileSize - iBytesWritten);
			iFileCluster = GetNextCluster(pDiskGeometryInfo, iFileCluster);
		}

		fclose(pFile);
	}
	else
	{
		printf("Cannot write %s (errno = %d)!\n", pFullFileName, errno);
	}

	return 0;
}

int ExtractDirectory(DiskGeometryInfo_t *pDiskGeometryInfo, int iDirectoryCluster, int iDepth, char *pFileWritePath)
{
	DirectoryEntry_t tDirectoryEntry;
	unsigned char *pDirectory;
	unsigned char *pDirectoryEnd;
	long lDirectoryOffset;

	while((iDirectoryCluster >= 0x002 && iDirectoryCluster <= 0xfef) || iDirectoryCluster == -1)
	{
		if(iDirectoryCluster == -1)
		{
			pDirectory =
				m_pDiskImage +
				(pDiskGeometryInfo->iReservedSectors +
				pDiskGeometryInfo->iSectorsPerFat * pDiskGeometryInfo->iNumberOfFats)
				* pDiskGeometryInfo->iBytesPerSector;

			pDirectoryEnd = pDirectory + 32 * pDiskGeometryInfo->iMaxNumberOfRootEntries;
		}
		else
		{
			lDirectoryOffset =
				(pDiskGeometryInfo->iReservedSectors +
				pDiskGeometryInfo->iSectorsPerFat * pDiskGeometryInfo->iNumberOfFats +
				(pDiskGeometryInfo->iMaxNumberOfRootEntries >> 4) +
				(iDirectoryCluster - 2) * pDiskGeometryInfo->iSectorsPerCluster) * pDiskGeometryInfo->iBytesPerSector;

			if(lDirectoryOffset >= ((pDiskGeometryInfo->iTotalSectors - pDiskGeometryInfo->iSectorsPerCluster) * pDiskGeometryInfo->iBytesPerSector))
			{
				printf("Directory broken (invalid cluster)!\n");

				break;
			}

			pDirectory = m_pDiskImage + lDirectoryOffset;
			pDirectoryEnd = pDirectory + pDiskGeometryInfo->iSectorsPerCluster * pDiskGeometryInfo->iBytesPerSector;
		}

		for(; (pDirectory < pDirectoryEnd) && *pDirectory; pDirectory += 32)
		{
			tDirectoryEntry = ReadDirectoryEntry(pDirectory);

			if(strcmp(tDirectoryEntry.tName, ".") == 0 || strcmp(tDirectoryEntry.tName, "..") == 0 || tDirectoryEntry.tName[0] == -27)
				continue;

			if(!IsNameValid(&tDirectoryEntry))
			{
				printf("Invalid name!\n");

				continue;
			}

			PrintDirectoryEntry(&tDirectoryEntry, iDepth);

			if(tDirectoryEntry.iAttributes & 0x10)
			{
				if(pFileWritePath)
				{
					char tFileWritePath[MAX_PATH_SIZE];

					strncpy(tFileWritePath, pFileWritePath, sizeof(tFileWritePath));
					strcat(tFileWritePath, tDirectoryEntry.tName);

					if(tDirectoryEntry.tExtension[0] != '\0')
					{
						strcat(tFileWritePath, ".");
						strcat(tFileWritePath, tDirectoryEntry.tExtension);
					}

					if(mkdir(tFileWritePath, S_IRWXU | S_IXGRP | S_IRWXO) != 0 && errno != EEXIST)
						printf("Cannot create %s (errno = %d)!\n", tFileWritePath, errno);

					strcat(tFileWritePath, "/");

					ExtractDirectory(pDiskGeometryInfo, tDirectoryEntry.iStartCluster, iDepth + 1, tFileWritePath);
				}
				else
				{
					ExtractDirectory(pDiskGeometryInfo, tDirectoryEntry.iStartCluster, iDepth + 1, NULL);
				}
			}
			else if(!(tDirectoryEntry.iAttributes & 0x08))
			{
				if(pFileWritePath)
				{
					char tFileWritePath[MAX_PATH_SIZE];

					strncpy(tFileWritePath, pFileWritePath, sizeof(tFileWritePath));
					strcat(tFileWritePath, tDirectoryEntry.tName);

					if(tDirectoryEntry.tExtension[0] != '\0')
					{
						strcat(tFileWritePath, ".");
						strcat(tFileWritePath, tDirectoryEntry.tExtension);
					}

					ExtractFile(pDiskGeometryInfo, tDirectoryEntry.iStartCluster, tFileWritePath, tDirectoryEntry.iSize);
				}
			}
		}

		if(iDirectoryCluster == -1 || *pDirectory == 0)
			break;

		iDirectoryCluster = GetNextCluster(pDiskGeometryInfo, iDirectoryCluster);
	}

	return 0;
}

void ReadMsaImageInfo(MsaImageInfo_t *pMsaImageInfo)
{
	pMsaImageInfo->iSectorSize = BYTES_PER_SECTOR;
	pMsaImageInfo->iSectorsPerTrack = READ_M16(m_pMsaImage, 2);
	pMsaImageInfo->iNumHeads = READ_M16(m_pMsaImage, 4) + 1;
	pMsaImageInfo->iStartTrack = READ_M16(m_pMsaImage, 6);
	pMsaImageInfo->iEndTrack = READ_M16(m_pMsaImage, 8);
	pMsaImageInfo->iNumTracks = pMsaImageInfo->iEndTrack + 1;
	pMsaImageInfo->iTotalSectors = pMsaImageInfo->iNumTracks * pMsaImageInfo->iSectorsPerTrack * pMsaImageInfo->iNumHeads;
}

void PrintMsaImageInfo(MsaImageInfo_t *pMsaImageInfo)
{
	printf("MSA image info:\n");
	printf("Sectors per track: %d\n", pMsaImageInfo->iSectorsPerTrack);
	printf("Number of heads: %d\n", pMsaImageInfo->iNumHeads);
	printf("Start track: %d\n", pMsaImageInfo->iStartTrack);
	printf("End track: %d\n", pMsaImageInfo->iEndTrack);
	printf("Number of tracks: %d\n", pMsaImageInfo->iNumTracks);
	printf("Total sectors: %d\n", pMsaImageInfo->iTotalSectors);
	printf("Total capacity: %d bytes\n", pMsaImageInfo->iTotalSectors * pMsaImageInfo->iSectorSize);
	printf("\n");
}

void ReadDiskGeometryInfo(DiskGeometryInfo_t *pDiskGeometryInfo)
{
	pDiskGeometryInfo->iBytesPerSector = READ_L16(m_pDiskImage, 11);
	pDiskGeometryInfo->iSectorsPerCluster = READ_L8(m_pDiskImage, 13);
	pDiskGeometryInfo->iReservedSectors = READ_L16(m_pDiskImage, 14);
	pDiskGeometryInfo->iNumberOfFats = READ_L8(m_pDiskImage, 16);
	pDiskGeometryInfo->iMaxNumberOfRootEntries = READ_L16(m_pDiskImage, 17);
	pDiskGeometryInfo->iTotalSectors = READ_L16(m_pDiskImage, 19);
	pDiskGeometryInfo->iMediaDescriptor = READ_L8(m_pDiskImage, 21);
	pDiskGeometryInfo->iSectorsPerFat = READ_L16(m_pDiskImage, 22);
	pDiskGeometryInfo->iSectorsPerTrack = READ_L16(m_pDiskImage, 24);
	pDiskGeometryInfo->iNumberOfSides = READ_L16(m_pDiskImage, 26);
	pDiskGeometryInfo->iNumberOfHiddenSectors = READ_L16(m_pDiskImage, 28);
}

int IsDiskGeometryInfoValid(DiskGeometryInfo_t *pDiskGeometryInfo)
{
	if(pDiskGeometryInfo->iBytesPerSector != BYTES_PER_SECTOR)
		return 0;

	if(pDiskGeometryInfo->iSectorsPerCluster != 1 &&
		pDiskGeometryInfo->iSectorsPerCluster != 2 &&
		pDiskGeometryInfo->iSectorsPerCluster != 4 &&
		pDiskGeometryInfo->iSectorsPerCluster != 8 &&
		pDiskGeometryInfo->iSectorsPerCluster != 16 &&
		pDiskGeometryInfo->iSectorsPerCluster != 32 &&
		pDiskGeometryInfo->iSectorsPerCluster != 64 &&
		pDiskGeometryInfo->iSectorsPerCluster != 128)
		return 0;

	if(pDiskGeometryInfo->iReservedSectors < 1)
		return 0;

	if(pDiskGeometryInfo->iNumberOfFats != 2)
		return 0;

	if(pDiskGeometryInfo->iMaxNumberOfRootEntries == 0)
		return 0;

//	pDiskGeometryInfo->iTotalSectors

	if(pDiskGeometryInfo->iMediaDescriptor < 0xf0)
		return 0;

	if(pDiskGeometryInfo->iSectorsPerFat == 0)
		return 0;

	if(pDiskGeometryInfo->iSectorsPerTrack == 0)
		return 0;

	if(pDiskGeometryInfo->iNumberOfSides == 0)
		return 0;

//	pDiskGeometryInfo->iNumberOfHiddenSectors

	return 1;
}

void PrintDiskGeometryInfo(DiskGeometryInfo_t *pDiskGeometryInfo)
{
	printf("Disk geometry info:\n");
	printf("Bytes per sector: %d\n", pDiskGeometryInfo->iBytesPerSector);
	printf("Sectors per cluster: %d\n", pDiskGeometryInfo->iSectorsPerCluster);
	printf("Reserved sectors: %d\n", pDiskGeometryInfo->iReservedSectors);
	printf("Number of FATs: %d\n", pDiskGeometryInfo->iNumberOfFats);
	printf("Maximum number of root entries: %d\n", pDiskGeometryInfo->iMaxNumberOfRootEntries);
	printf("Total sectors: %d\n", pDiskGeometryInfo->iTotalSectors);
	printf("Media descriptor: %02X\n", pDiskGeometryInfo->iMediaDescriptor);
	printf("Sectors per FAT: %d\n", pDiskGeometryInfo->iSectorsPerFat);
	printf("Sectors per track: %d\n", pDiskGeometryInfo->iSectorsPerTrack);
	printf("Number of sides: %d\n", pDiskGeometryInfo->iNumberOfSides);
	printf("Number of hidden sectors: %d\n", pDiskGeometryInfo->iNumberOfHiddenSectors);
	printf("\n");
}

int DecodeMsaImageToDiskImage(MsaImageInfo_t *pMsaImageInfo)
{
	unsigned char *pMsaPointer = m_pMsaImage + 10;
	unsigned char *pDiskPointer = m_pDiskImage + pMsaImageInfo->iStartTrack * pMsaImageInfo->iSectorsPerTrack * pMsaImageInfo->iNumHeads * pMsaImageInfo->iSectorSize;
	unsigned char *pEndPointer;
	int iTrackIndex;
	int iHeadIndex;
	int iNumBytes;
	unsigned char cMsaData;
	unsigned char cRleData;
	int iRleCount;

	for(iTrackIndex = pMsaImageInfo->iStartTrack; iTrackIndex <= pMsaImageInfo->iEndTrack; iTrackIndex++)
	{
		for(iHeadIndex = 0; iHeadIndex < pMsaImageInfo->iNumHeads; iHeadIndex++)
		{
			iNumBytes = (*pMsaPointer++ & 0xff) << 8;
			iNumBytes |= *pMsaPointer++ & 0xff;

			if(iNumBytes < pMsaImageInfo->iSectorsPerTrack * pMsaImageInfo->iSectorSize)
			{
				pEndPointer = pMsaPointer + iNumBytes;

				while(pMsaPointer < pEndPointer)
				{
					cMsaData = *pMsaPointer++;

					if(cMsaData != 0xe5)
					{
						*pDiskPointer++ = cMsaData;
					}
					else
					{
						cRleData = *pMsaPointer++;

						iRleCount = (*pMsaPointer++ & 0xff) << 8;
						iRleCount |= *pMsaPointer++ & 0xff;

						while(iRleCount)
						{
							*pDiskPointer++ = cRleData;
							iRleCount--;
						}
					}
				}
			}
			else
			{
				while(iNumBytes > 0)
				{
					*pDiskPointer++ = *pMsaPointer++;
					iNumBytes--;
				}
			}
		}
	}

	return 0;
}

int main(int argc, char **argv)
{
	struct stat tMsaFileInfoStruct;
	FILE *pMsaFile;
	char *pMsaFileName = NULL;
	MsaImageInfo_t tMsaImageInfo;
	DiskGeometryInfo_t tDiskGeometryInfo;
	int iIndex;
	int iMsaImageSize;
	int iDiskImageSize;

	// Process arguments.

	printf("MSA Tool v1.0 (" __DATE__ ") by Sascha Springer.\n");

	if(argc < 2 || argv[argc - 1][0] == '-')
	{
		printf("usage: %s [options] image.msa\n", argv[0]);

		PrintUsage();

		return 0;
	}

	ParseArguments(argc, argv);

	// Process MSA image.

	pMsaFileName = argv[argc - 1];

	pMsaFile = fopen(pMsaFileName, "rb");

	if(pMsaFile == NULL)
	{
		printf("Error reading %s!\n", pMsaFileName);

		return 1;
	}

	fstat(fileno(pMsaFile), &tMsaFileInfoStruct);
	m_pMsaImage = (unsigned char *)malloc((int )tMsaFileInfoStruct.st_size);

	if(!m_pMsaImage)
	{
		printf("Error: memory allocation for MSA image failed!\n");

		fclose(pMsaFile);

		return 1;
	}

	fread(m_pMsaImage, 1, (int )tMsaFileInfoStruct.st_size, pMsaFile);
	fclose(pMsaFile);

	if(!(m_pMsaImage[0] == 0x0e && m_pMsaImage[1] == 0x0f && m_pMsaImage[4] == 0 && m_pMsaImage[5] < 2))
	{
		printf("Error: %s is not an MSA file!\n", pMsaFileName);

		free(m_pMsaImage);

		return 1;
	}

	printf("Processing %s...\n\n", pMsaFileName);

	ReadMsaImageInfo(&tMsaImageInfo);

	iMsaImageSize = tMsaImageInfo.iTotalSectors * tMsaImageInfo.iSectorSize;

	m_pDiskImage = (unsigned char *)malloc(iMsaImageSize);

	if(!m_pDiskImage)
	{
		printf("Error: memory allocation for disk image failed!\n");

		free(m_pMsaImage);

		return 1;
	}

	memset(m_pDiskImage, 0, iMsaImageSize);

	DecodeMsaImageToDiskImage(&tMsaImageInfo);

	free(m_pMsaImage);

	ReadDiskGeometryInfo(&tDiskGeometryInfo);

	iDiskImageSize = tDiskGeometryInfo.iBytesPerSector * tDiskGeometryInfo.iTotalSectors;

	if(iDiskImageSize > iMsaImageSize)
	{
		m_pDiskImage = (unsigned char *)realloc(m_pDiskImage, iDiskImageSize);

		if(!m_pDiskImage)
		{
			printf("Error: memory reallocation for disk image failed!\n");

			return 1;
		}

		memset(m_pDiskImage + iMsaImageSize, 0, iDiskImageSize - iMsaImageSize);
	}

	if(IsArgumentSet("--show-info"))
	{
		printf("Showing MSA image info...\n");

		PrintMsaImageInfo(&tMsaImageInfo);
		PrintDiskGeometryInfo(&tDiskGeometryInfo);
	}

	if(IsArgumentSet("--convert-to-st"))
	{
		printf("Converting MSA image to ST image...\n");

		FILE *pStFile;
		char tStFileName[256];

		strncpy(tStFileName, GetArgumentValueString("--convert-to-st", ""), sizeof(tStFileName));

		if(strlen(tStFileName) == 0)
		{
			for(iIndex = strlen(pMsaFileName) - 1; iIndex >= 0 && pMsaFileName[iIndex] != '/'; iIndex--);

			if(iIndex >= 0)
				strcat(tStFileName, &pMsaFileName[iIndex + 1]);
			else
				strcat(tStFileName, pMsaFileName);

			for(iIndex = strlen(tStFileName) - 1; iIndex >= 0 && tStFileName[iIndex] != '.'; iIndex--);

			if(tStFileName[iIndex] == '.')
			{
				iIndex++;

				tStFileName[iIndex] = '\0';
				strcat(tStFileName, "st");
			}
		}

		pStFile = fopen(tStFileName, "wb");

		if(pStFile)
		{
			fwrite(m_pDiskImage, 1, iDiskImageSize, pStFile);
			fclose(pStFile);
		}
		else
		{
			printf("Error: cannot create ST image %s!\n", tStFileName);

			free(m_pDiskImage);

			return 1;
		}
	}

	if(IsArgumentSet("--extract-files"))
	{
		if(IsDiskGeometryInfoValid(&tDiskGeometryInfo))
		{
			char tFileWritePath[MAX_PATH_SIZE];

			printf("Extracting MSA image files...\n");

			strncpy(tFileWritePath, GetArgumentValueString("--extract-files", ""), sizeof(tFileWritePath));

			if(strlen(tFileWritePath) > 0 && tFileWritePath[strlen(tFileWritePath) - 1] != '/')
				strcat(tFileWritePath, "/");

			if(!IsArgumentSet("--omit-msa-folder"))
			{
				for(iIndex = strlen(pMsaFileName) - 1; iIndex >= 0 && pMsaFileName[iIndex] != '/'; iIndex--);

				if(iIndex >= 0)
					strcat(tFileWritePath, pMsaFileName + iIndex + 1);
				else
					strcat(tFileWritePath, pMsaFileName);

				for(iIndex = strlen(tFileWritePath) - 1; iIndex >= 0 && tFileWritePath[iIndex] != '/' && tFileWritePath[iIndex] != '.'; iIndex--);

				if(tFileWritePath[iIndex] == '.')
					tFileWritePath[iIndex] = '\0';

				if(mkdir(tFileWritePath, S_IRWXU | S_IXGRP | S_IRWXO) != 0 && errno != EEXIST)
					printf("Cannot create %s (errno = %d)!\n", tFileWritePath, errno);

				strcat(tFileWritePath, "/");
			}

			printf("%s\n", tFileWritePath);

			ExtractDirectory(&tDiskGeometryInfo, -1, 0, tFileWritePath);
		}
		else
		{
			printf("Error: cannot extract files (invalid disk geometry)!\n");

			free(m_pDiskImage);

			return 1;
		}
	}
	else if(IsArgumentSet("--list-files"))
	{
		if(IsDiskGeometryInfoValid(&tDiskGeometryInfo))
		{
			printf("Listing MSA image files...\n");

			ExtractDirectory(&tDiskGeometryInfo, -1, 0, NULL);
		}
		else
		{
			printf("Error: cannot list files (invalid disk geometry)!\n");

			free(m_pDiskImage);

			return 1;
		}
	}

	// Cleanup.

	free(m_pDiskImage);

	printf("\n");

	return 0;
}
